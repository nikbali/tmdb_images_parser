import com.mashape.unirest.http.exceptions.UnirestException;

/*
        Общий интерфейс для чтения данных по REST API TMDB
        здесь будут описаны основыне методы, которые необходимо будет реализовать для загрузки данных по API
        т.к в дальнейшем планируется загружать различные данные по категраиям(актеры, сериалы, фильмы, жанры) хочется иметь единый интерфейс для этой задачи
 */
public interface TMDBInputStream {
        String readData(String api_key) throws UnirestException;
        String readData() throws UnirestException;

}
