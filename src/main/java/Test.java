import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sun.xml.internal.bind.v2.TODO;
import javafx.scene.chart.PieChart;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;


public class Test {
    public static  void main(String [] args) {
        //лист в котором будем хранить объекты Film
        ArrayList<Film> films = new ArrayList<Film>();

        try
        {
            //создаем стрим для чтения с TMDB
          PopularMovieInputStream in = new PopularMovieInputStream();

          for(int k = 1; k <= 10; k++) //загружаем N страниц с популярными фильмами
          {
              //читаем в JSON объект(если сети нет, выкинет исключение)
              JSONObject q = new JSONObject(in.readData(k));
              JSONArray qq = q.getJSONArray("results");

              //преобразовываем в нашу структуру Film
              films.addAll(in.JsonArrayMoviesInputToFilm(qq));
          }

          //обратное преобразование из Films в JSON формат
          String json = in.FilmToJSON(films);

          //выводим на экран
          System.out.println(json);

           //записываем в файл
            OutputStream out = new FileOutputStream("D:\\films.json");
            out.write(json.getBytes());
            out.close();



        } catch (UnirestException ex) {
                ex.printStackTrace();
        }
        catch (FileNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}
