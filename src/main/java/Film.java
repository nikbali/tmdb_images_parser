import javafx.scene.input.DataFormat;

import java.util.ArrayList;

public class Film {
    int id;
    String title;
    String date;
    ArrayList <String> path_images  = new ArrayList<String>();

    public Film(int id, String title, String date,  ArrayList<String> path) {
        this.title = title;
        this.id = id;
        this.path_images = path;
        this.date = date;
    }
}
