import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PopularMovieInputStream implements TMDBInputStream {

    //TODO перегрузить еще метод readData c параметром на кол-во страниц популярных фильмов(сейчас загужает одну страницу т.е первые 20)

    public String readData(String api_key, int num_page) throws UnirestException {
        HttpResponse<String > response = Unirest.get("https://api.themoviedb.org/3/movie/popular?page="+num_page+"&language=ru&api_key=" + api_key)
                .asString();
        JSONObject e = new JSONObject(response.getBody());
        //JSONArray name = e.getJSONArray("results");
        return  e.toString();


    }

    public String readData() throws UnirestException {
        return readData("90c2cf6392b3f74022474d5a5e385061", 1);

    }

    public String readData(String api_key) throws UnirestException {
        return readData(api_key, 1);

    }
    public String readData(int num_page) throws UnirestException {
        return readData("90c2cf6392b3f74022474d5a5e385061", num_page);

    }

    public ArrayList<Film> JsonArrayMoviesInputToFilm(JSONArray movies) throws UnirestException
    {
        ArrayList<Film> films = new ArrayList<Film>();
        for (int i = 0; i < movies.length(); i++)
        {
            int id = movies.getJSONObject(i).getInt("id");
            String title = movies.getJSONObject(i).getString("title");
            String release_date = movies.getJSONObject(i).getString("release_date");
            ArrayList <String> photos = new ArrayList<String>();

            HttpResponse<String> response_images = Unirest.get("https://api.themoviedb.org/3/movie/" + id +"/images?api_key=90c2cf6392b3f74022474d5a5e385061")
                    .asString();
            JSONObject ima = new JSONObject(response_images.getBody());
            try
            {
                JSONArray ima_arr = ima.getJSONArray("backdrops");
                for (int j = 0; j < ima_arr.length(); j++)
                {
                    String path = ima_arr.getJSONObject(j).getString("file_path");
                    photos.add(path);
                }
                Film current_film = new Film(id, title, release_date, photos);
                films.add(current_film);
            }
            catch (JSONException ex)
            {
                System.out.println("Картинок нет, но я иду дальше");
            }

        }
        return  films;
    }

    public String FilmToJSON(ArrayList<Film> films)
    {
        JSONArray root_arr = new JSONArray(); // создаем главный объект
        int k = 0;
        for(Film i : films) {
            JSONObject root = new JSONObject();
            root.put("title", i.title);
            root.put("date", i.date);
            root.put("id", i.id);
            JSONArray node_photos = new JSONArray();
            for(int j = 0; j < i.path_images.size(); j++)
            {
                JSONObject photos = new JSONObject(); // создаем дочерний объект json для изображений
                photos.put("path_image", i.path_images.get(j));
                node_photos.put(j, photos);
            }
            root.put("images", node_photos); // сохраняем массив изображений
            root_arr.put(k, root);
            k++;
        }
        String json = root_arr.toString();
        return json;
    }

}
